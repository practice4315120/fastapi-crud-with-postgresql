from fastapi import FastAPI, status, HTTPException
from pydantic import BaseModel
from database import SessionLocal
from typing import Optional, List
import models

app = FastAPI()


class Item(BaseModel):  # serializer
    id: int
    name: str
    description: str
    price: float
    on_offer: bool

    class Config:
        orm_mode = True


db = SessionLocal()


@app.get('/items', response_model=List[Item], status_code=status.HTTP_200_OK)
def all_items():
    items = db.query(models.Item).all()
    return items


@app.get('/item/{item_id}', response_model=Item, status_code=status.HTTP_200_OK)
def item_by_id(item_id: int):
    item = db.query(models.Item).filter(models.Item.id == item_id).first()
    if item is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='resource not found')
    return item


@app.post('/item', response_model=Item, status_code=status.HTTP_201_CREATED)
def create_item(item: Item):
    item_odj = models.Item(
        name=item.name,
        description=item.description,
        price=item.price,
        on_offer=item.on_offer
    )
    db.add(item_odj)
    db.commit()
    return item_odj


@app.put('/item/{item_id}', response_model=Item, status_code=status.HTTP_200_OK)
def update_item(item_id: int, item: Item):
    check_item = db.query(models.Item).filter(models.Item.id == item_id).first()
    if check_item is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='resource not found')
    item_obj = db.query(models.Item).filter(models.Item.id == item_id).first()
    item_obj.name = item.name
    item_obj.description = item.description
    item_obj.price = item.price
    item_obj.on_offer = item.on_offer

    db.commit()
    return item_obj


@app.delete('/item/{item_id}')
def delete_item(item_id: int):
    item = db.query(models.Item).filter(models.Item.id == item_id).first()
    if item is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='resource not found')
    db.delete(item)
    db.commit()
    return {'msg': 'Item has been deleted'}
