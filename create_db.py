from database import Base, engine
from models import Item


print('migrating database tables...')
Base.metadata.create_all(engine)
