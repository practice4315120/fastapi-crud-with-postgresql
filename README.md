## Demo FastAPI CRUD With PostGreSQL

### This project used fastapi framework to create rest endpoints
### It is backed by PostgreSQL database

## Dependencies Used

#### fastapi -> the framework
#### sqlalchemy -> for ORM
#### psycopg2-binary -> python postgres database driver
#### uvicorn -> server