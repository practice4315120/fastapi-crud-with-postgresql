from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy import create_engine


# postgres database connection string
engine = create_engine('postgresql://root:root@127.0.0.1:5432/item_db', echo=True)
Base = declarative_base()
SessionLocal = sessionmaker(bind=engine)
